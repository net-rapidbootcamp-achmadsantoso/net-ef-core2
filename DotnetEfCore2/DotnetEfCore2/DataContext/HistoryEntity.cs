﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DotnetEfCore2.DataContext
{
    [Table("tbl_history")]
    public class HistoryEntity
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("file_name")]
        public string FileName { get; set; }

        [Column("description")]
        public string Description { get; set; }

        [Column("upload_date")]
        public string DateUpload { get; set; }

        [Column("generate_date")]
        public string DateGenerate { get; set; }

        [Column("upload_by")]
        public string UploadBy { get; set; }

        [Column("generate_by")]
        public string GenerateBy { get; set; }


        public HistoryEntity()
        {
        }

    }
}
