﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DotnetEfCore2.DataContext
{
    [Table("tbl_category")]
    public class SetupEntity
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("full_name")]
        public string FullName { get; set; }

        [Column("start_date")]
        public string StartDate { get; set; }

        [Column("end_date")]
        public string EndDate { get; set; }

        [Column("company")]
        public string Company { get; set; }

        [Column("report_to1")]
        public string ReportTo1 { get; set; }

        [Column("report_to2")]
        public string ReportTo2 { get; set; }

        public SetupEntity()
        {
        }

    }
}
