﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DotnetEfCore2.DataContext
{
    [Table("tbl_employee")]
    public class EmployeeEntity
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        
        [Column("full_name")]
        public String FullName { get; set; }

        [Column("email")]
        public String Email { get; set; }

        [Column("no_hp")]
        public long NoHp { get; set; }

        [Column("address")]
        public string Address { get; set; }

        [Column("position")]
        public String Position { get; set; }

        public EmployeeEntity()
        {
        }

    }
}
