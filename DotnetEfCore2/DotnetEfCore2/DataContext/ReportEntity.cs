﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DotnetEfCore2.DataContext
{
    [Table("tbl_report")]
    public class ReportEntity
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("full_name")]
        public string FullName { get; set; }

        [Column("date")]
        public string Date { get; set; }

        [Column("shift")]
        public string Shift { get; set; }

        [Column("in")]
        public string In { get; set; }

        [Column("out")]
        public string Out { get; set; }

        [Column("task")]
        public string Task { get; set; }

    
        public ReportEntity()
        {
        }

    }
}
