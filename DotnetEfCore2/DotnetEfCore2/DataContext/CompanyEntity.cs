﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DotnetEfCore2.DataContext
{
    [Table("tbl_company")]
    public class CompanyEntity
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("company_name")]
        public String CompanyName { get; set; }

        [Column("logo")]
        public String Logo { get; set; }

        [Column("address")]
        public string Address { get; set; }

        [Column("telp")]
        public long Telp { get; set; }

        
        public CompanyEntity()
        {
        }

    }
}
