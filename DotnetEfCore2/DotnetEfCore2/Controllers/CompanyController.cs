﻿using DotnetEfCore2.DataContext;
using Microsoft.AspNetCore.Mvc;

namespace DotnetEfCore2.Controllers
{
    public class CompanyController : Controller
    {

        private readonly ApplicationDbContext _context;
        public CompanyController(ApplicationDbContext context)
        { _context = context; }

        [HttpGet]
        public IActionResult GetAll()
        {
            IEnumerable<CompanyEntity> companies = _context.CompanyEntities.ToList();
            return View(companies);
        }

        [HttpGet]
        public IActionResult Details(int? id)
        {
            CompanyEntity company = _context.CompanyEntities.Find(id);
            return View(company);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Save([Bind("CompanyName, Logo, Address, Telp")] CompanyEntity request)
        {
            // add ke entity
            _context.CompanyEntities.Add(request);
            // commit to database
            _context.SaveChanges();

            return Redirect("GetAll");
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            var entity = _context.CompanyEntities.Find(id);
            return View(entity);
        }

        [HttpPost]
        public IActionResult Update([Bind("Id,CompanyName, Logo, Address, Telp")] CompanyEntity request)
        {
            // update entity
            _context.CompanyEntities.Update(request);
            // commit to database
            _context.SaveChanges();
            return Redirect("GetAll");
        }


        [HttpGet]
        public IActionResult Delete(int? id)
        {
            var entity = _context.CompanyEntities.Find(id);
            if (entity == null)
            {
                return Redirect("GetAll");
            }
            // remove from entity
            _context.CompanyEntities.Remove(entity);
            // commit to database
            _context.SaveChanges();
            return RedirectToAction("GetAll");
        }
    }
}
